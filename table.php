<?php
function table(array $table_data, array $table_head = NULL, array $table_foot = NULL, string $table_caption = NULL, float $table_border = 1, float $cell_spacing = 5, float $cell_padding = 5, string $table_class = "")
{
    $t_body = "";
    $t_foot = "";
    $t_head = <<<EOF
<table border="$table_border" cellpadding="$cell_padding" cellspacing="$cell_spacing" class="$table_class">

EOF;
    if($table_caption)
    {
        $t_head .= <<<EOF
    <caption>$table_caption</caption>

EOF;

    }
    if($table_head)
    {
        $t_head .= <<<EOF
    <thead>
        <tr>

EOF;
        foreach($table_head as $head)
        {
            $t_head .= <<<EOF
            <th> $head </th>

EOF;
        }
        $t_head .= <<<EOF
        </tr>
    </thead>

EOF;
    }

        $t_body .= <<<EOF
    <tbody>

EOF;
    foreach($table_data as $row_collection)
    {
        $t_body .= <<<EOF
        <tr>

EOF;
        foreach($row_collection as $row_data)
        {
            $t_body .= <<<EOF
            <td>$row_data</td>

EOF;
        }
        $t_body .= <<<EOF
        </tr>

EOF;

    }
    $t_body .= <<<EOF
    </tbody>

EOF;


    if($table_foot)
    {
        $t_foot .= <<<EOF
    <tfoot>
        <tr>

EOF;
        foreach($table_foot as $table_foot_data)
        {
            $t_foot .= <<<EOF
            <td>$table_foot_data</td>

EOF;
        }
        $t_foot .= <<<EOF
        </tr>
    </tfoot>

EOF;
    }
    $t_close = <<<EOF
</table>

EOF;
    return $t_head.$t_body.$t_foot.$t_close;
}