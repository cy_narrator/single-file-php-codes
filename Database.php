<?php

class Database extends mysqli
{
    private $SQL;
    private $db;
    private $table;
    private $result;
    public $LOG_FILE = "Database_log.txt";
    public $Debug = false;

    public function __construct(string $hostname = "localhost", string $username = "root", string $password = "", string $database = 'test', string $table = 'testtable', int $port = 3306)
    {
        parent::__construct($hostname, $username, $password, $database, $port);
        if(!$this->connect_error) {
            $this->db = $database;
            $this->table = $database.'.'.$table;
        }
        else{
            throw new Exception("Invalid connection in $this->table");
        }
    }

    private function string_cleaner($input)
    {
        ini_set('display_errors',0);
        $output = "";
        for($i = 0; $i <= strlen($input); $i++)
        {
            if(!($input[$i] == "\\"))
            {
                $output .= $input[$i];
            }
        }
        ini_set('display_errors',1);
        return $output;
    }


    public function execute($query_string = NULL)
    {
        if(!$query_string) {
            $this->SQL = $this->real_escape_string($this->SQL);
            $this->SQL = $this->string_cleaner($this->SQL);
            if($this->Debug)
            {
                $log_file = fopen($this->LOG_FILE, "a");
                fwrite($log_file, "About to execute: $this->SQL\n");
                fclose($log_file);
            }
            $this->result = $this->query($this->SQL);
            if(!$this->result)
            {
                $log_file = fopen($this->LOG_FILE, "a");
                fwrite($log_file, "    Error: $this->error\n");
                fclose($log_file);
                return -1;
            }
            else
            {
                return 0;
            }
        }

        else
        {
            $this->SQL = $this->real_escape_string($query_string);
            $this->SQL = $this->string_cleaner($this->SQL);
            if($this->Debug)
            {
                $log_file = fopen($this->LOG_FILE, "a");
                fwrite($log_file, "About to execute: $this->SQL\n");
                fclose($log_file);
            }
            $this->result = $this->query($this->SQL);
            if(!$this->result)
            {
                    $log_file = fopen($this->LOG_FILE, "a");
                    fwrite($log_file, "    Error: $this->error\n");
                    fclose($log_file);
                    return -1;
            }
            else
            {
                return 0;
            }
        }
    }

    public function switch_table($new_table)
    {
        @$this->table = $this->db.".".$new_table;
    }

    public function switch_db($new_db, $new_table = NULL)
    {
        if(!$new_table)
        {
            $this->db = $new_db;
            $this->table = $new_db.$this->table;
        }
        else
        {
            $this->db = $new_db;
            $this->table = $new_db.$new_table;
        }
    }

    public function close_connection()
    {
        $this->close();
    }

    public function delete_record($where_clause)
    {
        $this->SQL = "DELETE FROM $this->table WHERE $where_clause";
        $this->execute();
    }

    public function delete_all_data()
    {
        $this->SQL = "TRUNCATE TABLE $this->table";
        $this->execute();
    }

    public function edit_record($set_new_data, $where_old_data)
    {
        $this->SQL = "UPDATE $this->table SET $set_new_data WHERE $where_old_data";
        $this->execute();
    }

    public function delete_table()
    {
        $this->SQL = "DROP TABLE $this->table";
        $this->execute();
    }

    public function create_table($table_name, $rows)
    {
        $row = "";
        foreach($rows as $item)
        {
            $row .= $item;
            if(strlen($row) < count($rows)) {
                $row .= ",";
            }
        }
        $this->SQL = "CREATE TABLE $table_name($row)";
        $this->execute();
    }

    public function insert($table_values, $table_rows = NULL)
    {
        $table_value = '';
        $table_row = '';
        $value_index = 1;
        foreach($table_values as $item)
        {
            if(is_string($item))
            {
                $table_value .= '"'.$item.'"';
            }
            elseif(is_null($item)) {
                $table_value .= $item;
            }
            else
            {
                $table_value .= $item;
            }
            if($value_index != count($table_values)) {
                $table_value .= ", ";
            }
            $value_index += 1;
        }
        if(!$table_rows)
        {
            $this->SQL = "INSERT INTO $this->table VALUES($table_value)";
            $this->execute();
        }
        else
        {
            $row_index = 1;
            foreach($table_rows as $item)
            {
                $table_row .= $item;
                if($row_index != count($table_rows)) {
                    $table_row .= ", ";
                }

                $row_index += 1;
            }
            $this->SQL = "INSERT INTO $this->table($table_row) VALUES($table_value)";
            $this->execute();
        }
    }

    public function fetch($row_names = '*', $where_clause = NULL, $order_by_clause= NULL, $number_of_records = NULL)
    {
        if(!$where_clause && !$order_by_clause && !$number_of_records)
        {
            $this->SQL = "SELECT $row_names FROM $this->table";
            $this->execute();
        }

        elseif($where_clause && !$order_by_clause && !$number_of_records)
        {
            $this->SQL = "SELECT $row_names FROM $this->table WHERE $where_clause";
            $this->execute();
        }

        elseif($order_by_clause && !$where_clause && !$number_of_records)
        {
            $this->SQL = "SELECT $row_names FROM $this->table ORDER BY $order_by_clause";
            $this->execute();
        }

        elseif($number_of_records && !$where_clause && !$order_by_clause)
        {
            $this->SQL = "SELECT $row_names from $this->table LIMIT $number_of_records";
            $this->execute();
        }

        else
        {
            $this->SQL = "SELECT $row_names FROM $this->table WHERE $where_clause ORDER BY $order_by_clause LIMIT $number_of_records";
            $this -> execute();
        }
        if($this->result->num_rows > 0)
        {
            return $this->result->fetch_all();
        }
        else {
            return array();
        }
    }

}